import React from 'react';
import {Route, Switch} from "react-router-dom";
import Photos from "./containers/Photos/Photos";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import NewPhoto from "./containers/NewPhoto/NewPhoto";
import UserPhotos from "./containers/UserPhotos/UserPhotos";


const Routes = () => (
  <Switch>
    <Route path="/" exact component={Photos}/>
    <Route path="/photos/new" exact component={NewPhoto}/>
    <Route path="/user/:id" exact component={UserPhotos}/>
    <Route path="/register" exact component={Register}/>
    <Route path="/login" exact component={Login}/>
  </Switch>
);

export default Routes;