import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Image, Modal, PageHeader} from "react-bootstrap";
import {fetchUserPhotos, photoDelete} from "../../store/actions/photos";
import {Link} from "react-router-dom";

import UserPhotoListItem from '../../components/UserPhotoListItem/UserPhotoListItem';
import config from "../../config";

class UserPhotos extends Component {
  state = {
    show: false,
    photo: ''
  };

  componentDidMount() {
    this.props.onFetchUserPhotos(this.props.match.params.id);
    console.log(this.props.match.params)
  }

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = (img) => {
    this.setState({ show: true, photo: img });
  };

  deletePhoto = (id) => {
    this.props.onDeletePhoto(id);
  };

  render() {
    if (!this.props.photos) {
      return <div>Loading...</div>

    }
    let image = config.apiUrl + '/uploads/' + this.state.photo;

    return (
      <Fragment>
        <PageHeader>
          {this.props.match.params.user}'s Gallery
          { this.props.user &&
            <Link to="/photos/new">
              <Button bsStyle="primary" className="pull-right">
                Add new photo
              </Button>
            </Link>
          }
        </PageHeader>

        <Fragment>
        {this.props.photos.map(photo => (
          <UserPhotoListItem
            key={photo._id}
            id={photo._id}
            title={photo.title}
            image={photo.image}
            showModal={() => this.handleShow(photo.image)}
            onDelete={() => this.deletePhoto(photo._id)}
          />

        ))}
        </Fragment>

        <div>
        <Modal bsSize="large" show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
          </Modal.Header>
          <Col xs={12} md={8}>
            <Image
              responsive
              src={image}
            />
          </Col>
          <Modal.Footer>
            <Button onClick={this.handleClose}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    photos: state.photos.userPhotos,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchUserPhotos: (id) => dispatch(fetchUserPhotos(id)),
    onDeletePhoto: (id) => dispatch(photoDelete(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(UserPhotos);