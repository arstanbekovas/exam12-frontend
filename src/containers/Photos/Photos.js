import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Image, Modal, PageHeader} from "react-bootstrap";
import {fetchPhotos} from "../../store/actions/photos";
import {Link} from "react-router-dom";

import PhotoListItem from '../../components/PhotoListItem/PhotoListItem';
import config from "../../config";


class Photos extends Component {
  state = {
    show: false,
    photo: ''
  };

  componentDidMount() {
    this.props.onFetchPhotos();

  }

  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = (img) => {
    this.setState({ show: true, photo: img });
  };

  render() {
    let image = config.apiUrl + '/uploads/' + this.state.photo;

    return (
      <Fragment>
        <PageHeader>
          Gallery
          { this.props.user &&
            <Link to="/photos/new">
              <Button bsStyle="primary" className="pull-right">
                Add new photo
              </Button>
            </Link>
          }
        </PageHeader>

        {this.props.photos.map(photo => (
          <PhotoListItem
            key={photo._id}
            id={photo._id}
            title={photo.title}
            image={photo.image}
            user={photo.user.username}
            userId={photo.user._id}
            showModal={() => this.handleShow(photo.image)}
          />
        ))}

        <div>
        <Modal bsSize="large" show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
          </Modal.Header>
            <Col xs={12} md={8}>
            <Image
              responsive
              src={image}
              />
            </Col>
          <Modal.Footer>
            <Button onClick={this.handleClose}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    photos: state.photos.photos,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchPhotos: () => dispatch(fetchPhotos())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Photos);