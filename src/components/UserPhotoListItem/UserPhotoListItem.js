import React from 'react';
import {Button, Col, Image, Panel} from "react-bootstrap";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/images/not-found.png';

const UserPhotoListItem = props => {
  let image = notFound;

  if (props.image) {
    image = config.apiUrl + '/uploads/' + props.image;
  }

  return (

    <Col xs={6} md={4}>
      <Panel>
      <div onClick={props.showModal}>
        <Image
          src={image}
          responsive
        />
      </div>
      <div> Title:
        {props.title}
      </div>
      <Button
        style={{marginLeft: '10px', marginRight: '10px'}}
        bsStyle="danger"
        type="submit"
        onClick={props.onDelete}>Delete</Button>
      </Panel>
    </Col>

  );
};

UserPhotoListItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired
};

export default UserPhotoListItem;