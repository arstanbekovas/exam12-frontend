import React from 'react';
import {Col, Image, Panel} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/images/not-found.png';

const PhotoListItem = props => {
  let image = notFound;

  if (props.image) {
    image = config.apiUrl + '/uploads/' + props.image;
  }

  return (
      <Col xs={6} md={4}>
        <Panel>
        <div onClick={props.showModal}>
          <Image
            src={image}
            thumbnail
          />
        </div>
        <div> Title: {props.title} </div>
        <NavLink  to={'/user/' + props.userId}>
          <div> By: {props.user} </div>
        </NavLink>
        </Panel>
      </Col>
  );
};

PhotoListItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  userId: PropTypes.string
};

export default PhotoListItem;