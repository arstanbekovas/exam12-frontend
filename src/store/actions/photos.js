import axios from '../../axios-api';
import {push} from "react-router-redux";
import {
  CREATE_PHOTOS_SUCCESS, DELETE_PHOTO_SUCCESS, FETCH_PHOTOS_SUCCESS,
  FETCH_USER_PHOTOS_SUCCESS
} from "./actionTypes";
import {NotificationManager} from "react-notifications";

export const fetchPhotosSuccess = photos => {
  return {type: FETCH_PHOTOS_SUCCESS, photos};
};

export const fetchPhotos = () => {
  return dispatch => {
    axios.get('/photos').then(
      response => dispatch(fetchPhotosSuccess(response.data))
    );
  }
};

export const createPhotoSuccess = () => {
  return {type: CREATE_PHOTOS_SUCCESS};
};

export const createPhoto = photoData => {
  return (dispatch, getState) => {
    const headers = {'Token': getState().users.token};
    return axios.post('/photos', photoData, {headers}).then(
      response => {
        dispatch(createPhotoSuccess());
        dispatch(push('/'));
        NotificationManager.success('Success', 'New photo created successfully');
      },
      error => {
        NotificationManager.error('Error', 'Could not create new photo');
      }
    );
  };
};

export const fetchUserPhotosSuccess = (userPhotos) => {
  return {type: FETCH_USER_PHOTOS_SUCCESS, userPhotos};
};

export const fetchUserPhotos = (id) => {
  return (dispatch, getState)  => {
    const headers = {'Token': getState().users.token};
    return axios.get('/photos/user/' + id, {headers}).then(
      response => {
        dispatch(fetchUserPhotosSuccess(response.data))
      }
    );
  }
};

export const deletePhotoSuccess = photo => {
  return {type: DELETE_PHOTO_SUCCESS, photo};
};

export const photoDelete = (id) => {
  return (dispatch, getState) => {
    axios.delete('/photos/user/' + id, {headers: {'Token': getState().users.token}}).then(
      response => {
        dispatch(deletePhotoSuccess());
        dispatch(push('/'));
        NotificationManager.success('Success', 'Deleted successfully');
      },
      error => {
        NotificationManager.error('Error', 'Could not delete photo');
      }
    );
  };
};